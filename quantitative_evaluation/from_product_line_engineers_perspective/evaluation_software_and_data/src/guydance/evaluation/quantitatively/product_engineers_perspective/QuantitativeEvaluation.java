package guydance.evaluation.quantitatively.product_engineers_perspective;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import guydance.evaluation.quantitatively.product_engineers_perspective.evolution_operations.EvolutionOperation;
import guydance.evaluation.quantitatively.product_engineers_perspective.evolution_operations.ExtractFeature;
import guydance.evaluation.quantitatively.product_engineers_perspective.evolution_operations.MergeFeatures;
import guydance.evaluation.quantitatively.product_engineers_perspective.evolution_operations.RemoveFeatureWithArtifacts;

public class QuantitativeEvaluation {
	
	public static List<Configuration> readConfigurations(String basePath) {
		List<Configuration> configurations = new ArrayList<Configuration>(1000);
		
		for(int i=1;i<=1000;i++) {
			String configurationFileString = ""+i;
			
			while(configurationFileString.length() < 5) {
				configurationFileString = "0"+configurationFileString;
			}
			
			configurationFileString = configurationFileString+".config";
			
			File configurationFile = new File(basePath+configurationFileString);
			configurations.add(readConfiguration(configurationFile, i));
		}
		
		return configurations;
	}
	
	public static void performEvaluation(String basePath, List<EvolutionOperation> evolutionOperations) {
		basePath = "D:/workspaces/GuidanceEvaluation/guidance.evaluation.produt_line_engineer_perspective/"+basePath;
		
		File featureListFile = new File(basePath+"before/out.features");
		List<String> featureList = readFeatureList(featureListFile);
		List<Configuration> configurations = readConfigurations(basePath+"before/products/");

		List<Guidance> guidance = GuidanceChecker.checkGuidance(configurations, evolutionOperations);
		
//		File resultsFile = new File(basePath+"results.csv");
//		File resultsFileShort = new File(basePath+"results_short.csv");
		
		GuidanceFileWriter guidanceFileWriter = new GuidanceFileWriter();
		guidanceFileWriter.writeShortGuidanceResultsToCSV(featureList, configurations, evolutionOperations, guidance, basePath);
	}
	
	public static void main(String[] args) {
//		// Delete / Remove
//		
//		String basePathDeleteFeature7da62cb185 = "commits/remove/commit7da62cb185/";
//		RemoveFeatureWithArtifacts deleteFeature7da62cb185Operation = new RemoveFeatureWithArtifacts("I2C_NUC900");
//		List<EvolutionOperation> deleteFeature7da62cb185Operations = new ArrayList<EvolutionOperation>(1);
//		deleteFeature7da62cb185Operations.add(deleteFeature7da62cb185Operation);
//		
//		performEvaluation(basePathDeleteFeature7da62cb185, deleteFeature7da62cb185Operations);
//		
//		
//		String basePathDeleteFeaturefb5a515704 = "commits/remove/commitfb5a515704/";
//		List<EvolutionOperation> deleteFeaturefb5a515704Operations = new ArrayList<EvolutionOperation>(1);
//		deleteFeaturefb5a515704Operations.add(new RemoveFeatureWithArtifacts("PPC_WSP"));
//		
//		performEvaluation(basePathDeleteFeaturefb5a515704, deleteFeaturefb5a515704Operations);
//		
//		String basePathDeleteFeatureb633648c5a = "commits/remove/commitb633648c5a/";
//		List<EvolutionOperation> deleteFeatureb633648c5aOperations = new ArrayList<EvolutionOperation>(1);
//		deleteFeatureb633648c5aOperations.add(new RemoveFeatureWithArtifacts("MIPS_MT_SMTC"));
//		
//		performEvaluation(basePathDeleteFeatureb633648c5a, deleteFeatureb633648c5aOperations);
//		
//		String basePathDeleteFeature00d8521dcd = "commits/remove/commit00d8521dcd/";
//		List<EvolutionOperation> deleteFeature00d8521dcdOperations = new ArrayList<EvolutionOperation>(1);
//		deleteFeature00d8521dcdOperations.add(new RemoveFeatureWithArtifacts("RTS5139"));
//		
//		performEvaluation(basePathDeleteFeature00d8521dcd, deleteFeature00d8521dcdOperations);
//		
//		String basePathDeleteFeature8da4d6b2f7 = "commits/remove/commit8da4d6b2f7/";
//		List<EvolutionOperation> deleteFeature8da4d6b2f7Operations = new ArrayList<EvolutionOperation>(1);
//		deleteFeature8da4d6b2f7Operations.add(new RemoveFeatureWithArtifacts("MACH_EUKREA_MBIMXSD51_BASEBOARD"));
//		
//		performEvaluation(basePathDeleteFeature8da4d6b2f7, deleteFeature8da4d6b2f7Operations);
//		
//		String basePathDeleteFeaturedefd9da51d = "commits/remove/commitdefd9da51d/";
//		List<EvolutionOperation> deleteFeaturedefd9da51dOperations = new ArrayList<EvolutionOperation>(1);
//		deleteFeaturedefd9da51dOperations.add(new RemoveFeatureWithArtifacts("S3C2410_CLOCK"));
//		
//		performEvaluation(basePathDeleteFeaturedefd9da51d, deleteFeaturedefd9da51dOperations);
		
		
		// Extract
		
		 
//		String basePathExtractFeature995187bed3 = "commits/extract/commit995187bed3/";
//		List<EvolutionOperation> extractFeature995187bed3Operations = new ArrayList<EvolutionOperation>(1);
//		extractFeature995187bed3Operations.add(new ExtractFeature("IR_CORE"));
//		
//		performEvaluation(basePathExtractFeature995187bed3, extractFeature995187bed3Operations);
		
//		String basePathExtractFeaturef1d7dbbe = "commits/extract/commitf1d7dbbe/";
//		List<EvolutionOperation> extractFeaturef1d7dbbeOperations = new ArrayList<EvolutionOperation>(3);
//		extractFeaturef1d7dbbeOperations.add(new ExtractFeature("COMEDI"));
//		
//		performEvaluation(basePathExtractFeaturef1d7dbbe, extractFeaturef1d7dbbeOperations);
//		
//		String basePathExtractFeature347ec4e47d = "commits/extract/commit347ec4e47d/";
//		List<EvolutionOperation> extractFeature347ec4e47dOperations = new ArrayList<EvolutionOperation>(1);
//		extractFeature347ec4e47dOperations.add(new ExtractFeature("CPU_S5PV210"));
//		
//		performEvaluation(basePathExtractFeature347ec4e47d, extractFeature347ec4e47dOperations);
//		
//		String basePathExtractFeature5ee2b87779 = "commits/extract/commit5ee2b87779/";
//		List<EvolutionOperation> extractFeature5ee2b87779Operations = new ArrayList<EvolutionOperation>(1);
//		extractFeature5ee2b87779Operations.add(new ExtractFeature("ARCH_VEXPRESS"));
//		
//		performEvaluation(basePathExtractFeature5ee2b87779, extractFeature5ee2b87779Operations);
//		
//		String basePathExtractFeaturee2ca307439 = "commits/extract/commite2ca307439/";
//		List<EvolutionOperation> extractFeaturee2ca307439Operations = new ArrayList<EvolutionOperation>(1);
//		extractFeaturee2ca307439Operations.add(new ExtractFeature("I2C"));
//		
//		performEvaluation(basePathExtractFeaturee2ca307439, extractFeaturee2ca307439Operations);
//		
//		String basePathExtractFeatureeeb00c604a = "commits/extract/commiteeb00c604a/";
//		List<EvolutionOperation> extractFeatureeeb00c604aOperations = new ArrayList<EvolutionOperation>(1);
//		extractFeatureeeb00c604aOperations.add(new ExtractFeature("USB_GSPCA"));
//		
//		performEvaluation(basePathExtractFeatureeeb00c604a, extractFeatureeeb00c604aOperations);
//		
//		
//		// Merge
//		
//		
//		String basePathMergeFeatures8254baccdd = "commits/merge/commit8254baccdd/";
//		List<EvolutionOperation> mergeFeatures8254baccddOperations = new ArrayList<EvolutionOperation>(1);
//		mergeFeatures8254baccddOperations.add(new MergeFeatures("USB_U_MS", "USB_F_MASS_STORAGE"));
//		
//		performEvaluation(basePathMergeFeatures8254baccdd, mergeFeatures8254baccddOperations);
//		
//		String basePathMergeFeatures9c2b85f4f9 = "commits/merge/commit9c2b85f4f9/";
//		List<EvolutionOperation> mergeFeatures9c2b85f4f9Operations = new ArrayList<EvolutionOperation>(1);
//		mergeFeatures9c2b85f4f9Operations.add(new MergeFeatures("USB_U_RNDIS", "USB_F_RNDIS"));
//		
//		performEvaluation(basePathMergeFeatures9c2b85f4f9, mergeFeatures9c2b85f4f9Operations);
//		
//		String basePathMergeFeatures7c08c9ae0c = "commits/merge/commit7c08c9ae0c/";
//		List<EvolutionOperation> mergeFeatures7c08c9ae0cOperations = new ArrayList<EvolutionOperation>(1);
//		mergeFeatures7c08c9ae0cOperations.add(new MergeFeatures("FB_IMAC", "FB_EFI"));
//		
//		performEvaluation(basePathMergeFeatures7c08c9ae0c, mergeFeatures7c08c9ae0cOperations);
//		
//		String basePathMergeFeatures02729c3d08 = "commits/merge/commit02729c3d08/";
//		List<EvolutionOperation> mergeFeatures02729c3d08Operations = new ArrayList<EvolutionOperation>(1);
//		mergeFeatures02729c3d08Operations.add(new MergeFeatures("CAN_SJA1000_OF_PLATFORM", "CAN_SJA1000_PLATFORM"));
//		
//		performEvaluation(basePathMergeFeatures02729c3d08, mergeFeatures02729c3d08Operations);
//		
//		String basePathMergeFeatures396590b3bc = "commits/merge/commit396590b3bc/";
//		List<EvolutionOperation> mergeFeatures396590b3bcOperations = new ArrayList<EvolutionOperation>(1);
//		mergeFeatures396590b3bcOperations.add(new MergeFeatures("AD799X_RING_BUFFER", "AD799X"));
//		
//		performEvaluation(basePathMergeFeatures396590b3bc, mergeFeatures396590b3bcOperations);
//		
//		String basePathMergeFeaturesbc866fc7a8 = "commits/merge/commitbc866fc7a8/";
//		List<EvolutionOperation> mergeFeaturesbc866fc7a8Operations = new ArrayList<EvolutionOperation>(1);
//		mergeFeaturesbc866fc7a8Operations.add(new MergeFeatures("MFD_PM8XXX_IRQ", "MFD_PM8921_CORE"));
//		
//		performEvaluation(basePathMergeFeaturesbc866fc7a8, mergeFeaturesbc866fc7a8Operations);
	}
	
	
	private static List<String> readFeatureList(File file) {
		List<String> featureList = new ArrayList<String>();
		
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
		    String line = br.readLine();

		    while (line != null) {
		        featureList.add(line.substring(7));
		        line = br.readLine();
		    }
		    
//		    String everything = sb.toString();
		    
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
		    try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return featureList;
	}
	
	
	private static Configuration readConfiguration(File file, int configurationId) {	
		Configuration configuration = new Configuration();
    	configuration.setConfigurationId(configurationId);
    	configuration.setSelectedFeatures(new ArrayList<String>());

		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
		    String line = br.readLine();

		    while (line != null) {
		        configuration.getSelectedFeatures().add(line);

		        line = br.readLine();
		    }
		    
//		    String everything = sb.toString();
		    
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
		    try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return configuration;
	}
}
