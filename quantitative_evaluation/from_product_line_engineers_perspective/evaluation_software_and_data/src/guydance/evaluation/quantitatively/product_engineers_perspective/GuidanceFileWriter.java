package guydance.evaluation.quantitatively.product_engineers_perspective;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import guydance.evaluation.quantitatively.product_engineers_perspective.evolution_operations.EvolutionOperation;
import guydance.evaluation.quantitatively.product_engineers_perspective.evolution_operations.ExtractFeature;
import guydance.evaluation.quantitatively.product_engineers_perspective.evolution_operations.MergeFeatures;
import guydance.evaluation.quantitatively.product_engineers_perspective.evolution_operations.RemoveFeatureWithArtifacts;

public class GuidanceFileWriter {

	public void writeShortGuidanceResultsToCSV(List<String> featureList, List<Configuration> configurations,
			List<EvolutionOperation> evolutionOperations, List<Guidance> guidanceList, String baseFileName) {

		List<MergeFeatures> mergeFeaturesOperations = new ArrayList<MergeFeatures>();
		List<ExtractFeature> extractFeatureOperations = new ArrayList<ExtractFeature>();
		List<RemoveFeatureWithArtifacts> removeFeatureWithArtifactOperations = new ArrayList<RemoveFeatureWithArtifacts>();

		for (EvolutionOperation evoOp : evolutionOperations) {
			if (evoOp instanceof MergeFeatures) {
				mergeFeaturesOperations.add((MergeFeatures) evoOp);
			} else if (evoOp instanceof ExtractFeature) {
				extractFeatureOperations.add((ExtractFeature) evoOp);
			} else if (evoOp instanceof RemoveFeatureWithArtifacts) {
				removeFeatureWithArtifactOperations.add((RemoveFeatureWithArtifacts) evoOp);
			}
		}

		List<Guidance> mergeGuidance = new ArrayList<Guidance>(guidanceList.size() / 3);
		List<Guidance> extractGuidance = new ArrayList<Guidance>(guidanceList.size() / 3);
		List<Guidance> deleteGuidance = new ArrayList<Guidance>(guidanceList.size() / 3);

		for (Guidance guidance : guidanceList) {
			EvolutionOperation evoOp = guidance.getEvolutionOperation();
			if (evoOp instanceof MergeFeatures) {
				mergeGuidance.add(guidance);
			} else if (evoOp instanceof ExtractFeature) {
				extractGuidance.add(guidance);
			} else if (evoOp instanceof RemoveFeatureWithArtifacts) {
				deleteGuidance.add(guidance);
			}
		}

		if(mergeFeaturesOperations != null && !mergeFeaturesOperations.isEmpty()) {			
			writeShortGuidanceResultsToCSVMerge(featureList, configurations, mergeFeaturesOperations, mergeGuidance,
					baseFileName);
		}
		
		if(extractFeatureOperations != null && !extractFeatureOperations.isEmpty()) {
			writeShortGuidanceResultsToCSVExtract(featureList, configurations, extractFeatureOperations, extractGuidance,
					baseFileName);
		}
		
		if(removeFeatureWithArtifactOperations != null && !removeFeatureWithArtifactOperations.isEmpty()) {
			writeShortGuidanceResultsToCSVDelete(featureList, configurations, removeFeatureWithArtifactOperations,
					deleteGuidance, baseFileName);
		}
	}
	
	private void writeShortGuidanceResultsToCSVDelete(List<String> featureList, List<Configuration> configurations,
			List<RemoveFeatureWithArtifacts> evolutionOperations, List<Guidance> guidanceList, String baseFilePath) {
		String[][] matrix = new String[3][evolutionOperations.size() + 1];
		matrix[0][0] = "";
		matrix[1][0] = "D0";
		matrix[2][0] = "D1";
		
		Map<RemoveFeatureWithArtifacts, List<Guidance>> theMap = new HashMap<RemoveFeatureWithArtifacts, List<Guidance>>();
		
		for(RemoveFeatureWithArtifacts extractFeature: evolutionOperations) {
			theMap.put(extractFeature, new ArrayList<Guidance>(configurations.size()));
		}
		
		for(Guidance guidance : guidanceList) {
			theMap.get(guidance.getEvolutionOperation()).add(guidance);
		}
		
		for(int i=0;i<evolutionOperations.size();i++) {
			RemoveFeatureWithArtifacts deleteFeature = evolutionOperations.get(i);
			
			List<Guidance> guidances = theMap.get(deleteFeature);
			double d0 = 0;
			double d1 = 0;
			
			for(Guidance guidance : guidances) {
				String guidanceId = guidance.getGuidanceIdFromPaper();
				if(guidanceId.equals("D0")) {
					d0++;
				}
				else if(guidanceId.equals("D1")) {
					d1++;
				}
			}
			
			matrix[0][i+1] = "delete_"+i;
			matrix[1][i+1] = ""+(d0/configurations.size()*100);
			matrix[2][i+1] = ""+(d1/configurations.size()*100);
		}
		
		File file = new File(baseFilePath + "results_delete_short.csv");

		

		try {
			PrintWriter fileWriter = new PrintWriter(file, "UTF-8");

			for (int i = 0; i <= evolutionOperations.size(); i++) {
				StringBuilder lineBuilder = new StringBuilder();
				
				for(int j=0;j<=2;j++) {					
					if (j != 0) {
						lineBuilder.append(";");
					}
					
					lineBuilder.append(matrix[j][i]);
				}
				
				
				fileWriter.println(lineBuilder.toString());
			}
			
			fileWriter.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	private void writeShortGuidanceResultsToCSVMerge(List<String> featureList, List<Configuration> configurations,
			List<MergeFeatures> evolutionOperations, List<Guidance> guidanceList, String baseFilePath) {
		String[][] matrix = new String[5][evolutionOperations.size() + 1];
		matrix[0][0] = "";
		matrix[1][0] = "M0";
		matrix[2][0] = "M1";
		matrix[3][0] = "M2";
		matrix[4][0] = "M3";
		
		Map<MergeFeatures, List<Guidance>> theMap = new HashMap<MergeFeatures, List<Guidance>>();
		
		for(MergeFeatures mergeFeature: evolutionOperations) {
			theMap.put(mergeFeature, new ArrayList<Guidance>(configurations.size()));
		}
		
		for(Guidance guidance : guidanceList) {
			theMap.get(guidance.getEvolutionOperation()).add(guidance);
		}
		
		for(int i=0;i<evolutionOperations.size();i++) {
			MergeFeatures mergeFeatures = evolutionOperations.get(i);
			
			List<Guidance> guidances = theMap.get(mergeFeatures);
			double m0 = 0;
			double m1 = 0;
			double m2 = 0;
			double m3 = 0;
			
			for(Guidance guidance : guidances) {
				String guidanceId = guidance.getGuidanceIdFromPaper();
				if(guidanceId.equals("M0")) {
					m0++;
				}
				else if(guidanceId.equals("M1")) {
					m1++;
				}

				else if(guidanceId.equals("M2")) {
					m2++;
				}

				else if(guidanceId.equals("M3")) {
					m3++;
				}
			}
			
			matrix[0][i+1] = "merge_"+i;
			matrix[1][i+1] = ""+(m0/configurations.size()*100);
			matrix[2][i+1] = ""+(m1/configurations.size()*100);
			matrix[3][i+1] = ""+(m2/configurations.size()*100);
			matrix[4][i+1] = ""+(m3/configurations.size()*100);
		}
		
		File file = new File(baseFilePath + "results_merge_short.csv");

		

		try {
			PrintWriter fileWriter = new PrintWriter(file, "UTF-8");

			for (int i = 0; i <= evolutionOperations.size(); i++) {
				StringBuilder lineBuilder = new StringBuilder();
				
				for(int j=0;j<=4;j++) {					
					if (j != 0) {
						lineBuilder.append(";");
					}
					
					lineBuilder.append(matrix[j][i]);
				}
				
				
				fileWriter.println(lineBuilder.toString());
			}
			
			fileWriter.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	private void writeShortGuidanceResultsToCSVExtract(List<String> featureList, List<Configuration> configurations,
			List<ExtractFeature> evolutionOperations, List<Guidance> guidanceList, String baseFilePath) {
		String[][] matrix = new String[3][evolutionOperations.size() + 1];
		matrix[0][0] = "";
		matrix[1][0] = "E0";
		matrix[2][0] = "E1";
		
		Map<ExtractFeature, List<Guidance>> theMap = new HashMap<ExtractFeature, List<Guidance>>();
		
		for(ExtractFeature extractFeature: evolutionOperations) {
			theMap.put(extractFeature, new ArrayList<Guidance>(configurations.size()));
		}
		
		for(Guidance guidance : guidanceList) {
			theMap.get(guidance.getEvolutionOperation()).add(guidance);
		}
		
		for(int i=0;i<evolutionOperations.size();i++) {
			ExtractFeature extractFeatures = evolutionOperations.get(i);
			
			List<Guidance> guidances = theMap.get(extractFeatures);
			double e0 = 0;
			double e1 = 0;
			
			for(Guidance guidance : guidances) {
				String guidanceId = guidance.getGuidanceIdFromPaper();
				if(guidanceId.equals("E0")) {
					e0++;
				}
				else if(guidanceId.equals("E1")) {
					e1++;
				}
			}
			
			matrix[0][i+1] = "extract_"+i;
			matrix[1][i+1] = ""+(e0/configurations.size()*100);
			matrix[2][i+1] = ""+(e1/configurations.size()*100);
		}
		
		File file = new File(baseFilePath + "results_extract_short.csv");

		

		try {
			PrintWriter fileWriter = new PrintWriter(file, "UTF-8");

			for (int i = 0; i <= evolutionOperations.size(); i++) {
				StringBuilder lineBuilder = new StringBuilder();
				
				for(int j=0;j<=2;j++) {					
					if (j != 0) {
						lineBuilder.append(";");
					}
					
					lineBuilder.append(matrix[j][i]);
				}
				
				
				fileWriter.println(lineBuilder.toString());
			}
			
			fileWriter.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
