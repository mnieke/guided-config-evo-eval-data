package guydance.evaluation.quantitatively.product_engineers_perspective;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import guydance.evaluation.quantitatively.product_engineers_perspective.evolution_operations.EvolutionOperation;
import guydance.evaluation.quantitatively.product_engineers_perspective.evolution_operations.ExtractFeature;
import guydance.evaluation.quantitatively.product_engineers_perspective.evolution_operations.MergeFeatures;
import guydance.evaluation.quantitatively.product_engineers_perspective.evolution_operations.RemoveFeatureWithArtifacts;

public class GuidanceFileWriter {

	public void writeGuidanceToCSV(List<String> featureModelList, List<Configuration> configurations,
			List<EvolutionOperation> evolutionOperations, List<Guidance> guidanceList, String baseFilePath) {

		List<MergeFeatures> mergeFeatureOperations = new ArrayList<MergeFeatures>();
		List<ExtractFeature> extractFeatureOperations = new ArrayList<ExtractFeature>();
		List<RemoveFeatureWithArtifacts> removeFeatureWithArtifactOperations = new ArrayList<RemoveFeatureWithArtifacts>();

		for (EvolutionOperation evoOp : evolutionOperations) {
			if (evoOp instanceof MergeFeatures) {
				mergeFeatureOperations.add((MergeFeatures) evoOp);
			} else if (evoOp instanceof ExtractFeature) {
				extractFeatureOperations.add((ExtractFeature) evoOp);
			} else if (evoOp instanceof RemoveFeatureWithArtifacts) {
				removeFeatureWithArtifactOperations.add((RemoveFeatureWithArtifacts) evoOp);
			}
		}

		String[][] mergeFeatureMatrix = new String[configurations.size() + 1][mergeFeatureOperations.size() + 1];
		String[][] extractFeatureMatrix = new String[configurations.size() + 1][extractFeatureOperations.size() + 1];
		String[][] removeFeatureWithArtifactMatrix = new String[configurations.size()
				+ 1][removeFeatureWithArtifactOperations.size() + 1];

		mergeFeatureMatrix[0][0] = "";
		extractFeatureMatrix[0][0] = "";
		removeFeatureWithArtifactMatrix[0][0] = "";

		for (int i = 0; i < configurations.size(); i++) {
			mergeFeatureMatrix[i + 1][0] = "" + configurations.get(i).getConfigurationId();
			extractFeatureMatrix[i + 1][0] = "" + configurations.get(i).getConfigurationId();
			removeFeatureWithArtifactMatrix[i + 1][0] = "" + configurations.get(i).getConfigurationId();
		}

		for (int i = 0; i < mergeFeatureOperations.size(); i++) {
			mergeFeatureMatrix[0][i + 1] = "merge_" + i;
		}

		for (int i = 0; i < extractFeatureOperations.size(); i++) {
			extractFeatureMatrix[0][i + 1] = "extract_" + i;
		}

		for (int i = 0; i < removeFeatureWithArtifactOperations.size(); i++) {
			removeFeatureWithArtifactMatrix[0][i + 1] = "delete_" + i;
		}

		for (Guidance guidance : guidanceList) {
			int x = configurations.indexOf(guidance.getConfiguration()) + 1;
			int y = evolutionOperations.indexOf(guidance.getEvolutionOperation()) + 1;

			String[][] matrix = null;

			EvolutionOperation evoOp = guidance.getEvolutionOperation();
			if (evoOp instanceof MergeFeatures) {
				matrix = mergeFeatureMatrix;
				y = mergeFeatureOperations.indexOf(guidance.getEvolutionOperation()) + 1;
			} else if (evoOp instanceof ExtractFeature) {
				matrix = extractFeatureMatrix;
				y = extractFeatureOperations.indexOf(guidance.getEvolutionOperation()) + 1;
			} else if (evoOp instanceof RemoveFeatureWithArtifacts) {
				matrix = removeFeatureWithArtifactMatrix;
				y = removeFeatureWithArtifactOperations.indexOf(guidance.getEvolutionOperation()) + 1;
			}

			matrix[x][y] = guidance.getGuidanceIdFromPaper();
		}

		try {

			File mergeFile = new File(baseFilePath + "_merge.csv");
			File extractFile = new File(baseFilePath + "_extract.csv");
			File deleteFile = new File(baseFilePath + "_delete.csv");

			PrintWriter mergeWriter = new PrintWriter(mergeFile, "UTF-8");
			PrintWriter extractWriter = new PrintWriter(extractFile, "UTF-8");
			PrintWriter deleteWriter = new PrintWriter(deleteFile, "UTF-8");

			for (int i = 0; i <= mergeFeatureOperations.size(); i++) {
				StringBuilder lineBuilder = new StringBuilder();

				for (int j = 0; j <= configurations.size(); j++) {

					if (j != 0) {
						lineBuilder.append(";");
					}

					lineBuilder.append(mergeFeatureMatrix[j][i]);
				}

				mergeWriter.println(lineBuilder.toString());
			}

			for (int i = 0; i <= extractFeatureOperations.size(); i++) {
				StringBuilder lineBuilder = new StringBuilder();

				for (int j = 0; j <= configurations.size(); j++) {

					if (j != 0) {
						lineBuilder.append(";");
					}

					lineBuilder.append(extractFeatureMatrix[j][i]);
				}

				extractWriter.println(lineBuilder.toString());
			}

			for (int i = 0; i <= removeFeatureWithArtifactOperations.size(); i++) {
				StringBuilder lineBuilder = new StringBuilder();

				for (int j = 0; j <= configurations.size(); j++) {

					if (j != 0) {
						lineBuilder.append(";");
					}

					lineBuilder.append(removeFeatureWithArtifactMatrix[j][i]);
				}

				deleteWriter.println(lineBuilder.toString());
			}

			// for(Guidance guidance: guidanceList) {
			// StringBuilder lineBuilder = new StringBuilder();
			// lineBuilder.append(guidance.getConfiguration().getConfigurationId());
			// lineBuilder.append(";");
			//
			// EvolutionOperation evoOp = guidance.getEvolutionOperation();
			// String evoOpString = "";
			// if(evoOp instanceof MergeFeatures) {
			// evoOpString = "merge";
			// }
			// else if(evoOp instanceof ExtractFeature) {
			// evoOpString = "extract";
			// }
			// else if(evoOp instanceof RemoveFeatureWithArtifacts) {
			// evoOpString = "delete";
			// }
			// lineBuilder.append(evoOpString);
			// lineBuilder.append(";");
			//
			// lineBuilder.append(guidance.getGuidanceIdFromPaper());
			// lineBuilder.append(";");
			//
			// lineBuilder.append(guidance.isBehaviorPreserving());
			//
			// writer.println(lineBuilder.toString());
			// }

			mergeWriter.close();
			extractWriter.close();
			deleteWriter.close();
		} catch (IOException e) {
			// do something
		}
	}

	public void writeShortGuidanceResultsToCSV(List<String> featureList, List<Configuration> configurations,
			List<EvolutionOperation> evolutionOperations, List<Guidance> guidanceList, String baseFileName) {

		List<MergeFeatures> mergeFeaturesOperations = new ArrayList<MergeFeatures>();
		List<ExtractFeature> extractFeatureOperations = new ArrayList<ExtractFeature>();
		List<RemoveFeatureWithArtifacts> removeFeatureWithArtifactOperations = new ArrayList<RemoveFeatureWithArtifacts>();

		for (EvolutionOperation evoOp : evolutionOperations) {
			if (evoOp instanceof MergeFeatures) {
				mergeFeaturesOperations.add((MergeFeatures) evoOp);
			} else if (evoOp instanceof ExtractFeature) {
				extractFeatureOperations.add((ExtractFeature) evoOp);
			} else if (evoOp instanceof RemoveFeatureWithArtifacts) {
				removeFeatureWithArtifactOperations.add((RemoveFeatureWithArtifacts) evoOp);
			}
		}

		List<Guidance> mergeGuidance = new ArrayList<Guidance>(guidanceList.size() / 3);
		List<Guidance> extractGuidance = new ArrayList<Guidance>(guidanceList.size() / 3);
		List<Guidance> deleteGuidance = new ArrayList<Guidance>(guidanceList.size() / 3);

		for (Guidance guidance : guidanceList) {
			EvolutionOperation evoOp = guidance.getEvolutionOperation();
			if (evoOp instanceof MergeFeatures) {
				mergeGuidance.add(guidance);
			} else if (evoOp instanceof ExtractFeature) {
				extractGuidance.add(guidance);
			} else if (evoOp instanceof RemoveFeatureWithArtifacts) {
				deleteGuidance.add(guidance);
			}
		}

		writeShortGuidanceResultsToCSVMerge(featureList, configurations, mergeFeaturesOperations, mergeGuidance,
				baseFileName);
		writeShortGuidanceResultsToCSVExtract(featureList, configurations, extractFeatureOperations, extractGuidance,
				baseFileName);
		writeShortGuidanceResultsToCSVDelete(featureList, configurations, removeFeatureWithArtifactOperations,
				deleteGuidance, baseFileName);
	}

	private void writeShortGuidanceResultsToCSVMerge(List<String> featureList, List<Configuration> configurations,
			List<MergeFeatures> evolutionOperations, List<Guidance> guidanceList, String baseFilePath) {
		String[][] matrix = new String[5][evolutionOperations.size() + 1];
		matrix[0][0] = "";
		matrix[1][0] = "M0";
		matrix[2][0] = "M1";
		matrix[3][0] = "M2";
		matrix[4][0] = "M3";
		
		Map<MergeFeatures, List<Guidance>> theMap = new HashMap<MergeFeatures, List<Guidance>>();
		
		for(MergeFeatures mergeFeature: evolutionOperations) {
			theMap.put(mergeFeature, new ArrayList<Guidance>(configurations.size()));
		}
		
		for(Guidance guidance : guidanceList) {
			theMap.get(guidance.getEvolutionOperation()).add(guidance);
		}
		
		for(int i=0;i<evolutionOperations.size();i++) {
			MergeFeatures mergeFeatures = evolutionOperations.get(i);
			
			List<Guidance> guidances = theMap.get(mergeFeatures);
			double m0 = 0;
			double m1 = 0;
			double m2 = 0;
			double m3 = 0;
			
			for(Guidance guidance : guidances) {
				String guidanceId = guidance.getGuidanceIdFromPaper();
				if(guidanceId.equals("M0")) {
					m0++;
				}
				else if(guidanceId.equals("M1")) {
					m1++;
				}

				else if(guidanceId.equals("M2")) {
					m2++;
				}

				else if(guidanceId.equals("M3")) {
					m3++;
				}
			}
			
			matrix[0][i+1] = "merge_"+i;
			matrix[1][i+1] = ""+(m0/configurations.size()*100);
			matrix[2][i+1] = ""+(m1/configurations.size()*100);
			matrix[3][i+1] = ""+(m2/configurations.size()*100);
			matrix[4][i+1] = ""+(m3/configurations.size()*100);
		}
		
		File file = new File(baseFilePath + "_merge_short.csv");

		

		try {
			PrintWriter fileWriter = new PrintWriter(file, "UTF-8");

			for (int i = 0; i <= evolutionOperations.size(); i++) {
				StringBuilder lineBuilder = new StringBuilder();
				
				for(int j=0;j<=4;j++) {					
					if (j != 0) {
						lineBuilder.append(";");
					}
					
					lineBuilder.append(matrix[j][i]);
				}
				
				
				fileWriter.println(lineBuilder.toString());
			}
			
			fileWriter.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	private void writeShortGuidanceResultsToCSVExtract(List<String> featureList, List<Configuration> configurations,
			List<ExtractFeature> evolutionOperations, List<Guidance> guidanceList, String baseFilePath) {
		String[][] matrix = new String[3][evolutionOperations.size() + 1];
		matrix[0][0] = "";
		matrix[1][0] = "E0";
		matrix[2][0] = "E1";
		
		Map<ExtractFeature, List<Guidance>> theMap = new HashMap<ExtractFeature, List<Guidance>>();
		
		for(ExtractFeature extractFeature: evolutionOperations) {
			theMap.put(extractFeature, new ArrayList<Guidance>(configurations.size()));
		}
		
		for(Guidance guidance : guidanceList) {
			theMap.get(guidance.getEvolutionOperation()).add(guidance);
		}
		
		for(int i=0;i<evolutionOperations.size();i++) {
			ExtractFeature extractFeatures = evolutionOperations.get(i);
			
			List<Guidance> guidances = theMap.get(extractFeatures);
			double e0 = 0;
			double e1 = 0;
			
			for(Guidance guidance : guidances) {
				String guidanceId = guidance.getGuidanceIdFromPaper();
				if(guidanceId.equals("E0")) {
					e0++;
				}
				else if(guidanceId.equals("E1")) {
					e1++;
				}
			}
			
			matrix[0][i+1] = "extract_"+i;
			matrix[1][i+1] = ""+(e0/configurations.size()*100);
			matrix[2][i+1] = ""+(e1/configurations.size()*100);
		}
		
		File file = new File(baseFilePath + "_extract_short.csv");

		

		try {
			PrintWriter fileWriter = new PrintWriter(file, "UTF-8");

			for (int i = 0; i <= evolutionOperations.size(); i++) {
				StringBuilder lineBuilder = new StringBuilder();
				
				for(int j=0;j<=2;j++) {					
					if (j != 0) {
						lineBuilder.append(";");
					}
					
					lineBuilder.append(matrix[j][i]);
				}
				
				
				fileWriter.println(lineBuilder.toString());
			}
			
			fileWriter.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void writeShortGuidanceResultsToCSVDelete(List<String> featureList, List<Configuration> configurations,
			List<RemoveFeatureWithArtifacts> evolutionOperations, List<Guidance> guidanceList, String baseFilePath) {
		String[][] matrix = new String[3][evolutionOperations.size() + 1];
		matrix[0][0] = "";
		matrix[1][0] = "D0";
		matrix[2][0] = "D1";
		
		Map<RemoveFeatureWithArtifacts, List<Guidance>> theMap = new HashMap<RemoveFeatureWithArtifacts, List<Guidance>>();
		
		for(RemoveFeatureWithArtifacts extractFeature: evolutionOperations) {
			theMap.put(extractFeature, new ArrayList<Guidance>(configurations.size()));
		}
		
		for(Guidance guidance : guidanceList) {
			theMap.get(guidance.getEvolutionOperation()).add(guidance);
		}
		
		for(int i=0;i<evolutionOperations.size();i++) {
			RemoveFeatureWithArtifacts deleteFeature = evolutionOperations.get(i);
			
			List<Guidance> guidances = theMap.get(deleteFeature);
			double d0 = 0;
			double d1 = 0;
			
			for(Guidance guidance : guidances) {
				String guidanceId = guidance.getGuidanceIdFromPaper();
				if(guidanceId.equals("D0")) {
					d0++;
				}
				else if(guidanceId.equals("D1")) {
					d1++;
				}
			}
			
			matrix[0][i+1] = "delete_"+i;
			matrix[1][i+1] = ""+(d0/configurations.size()*100);
			matrix[2][i+1] = ""+(d1/configurations.size()*100);
		}
		
		File file = new File(baseFilePath + "_delete_short.csv");

		

		try {
			PrintWriter fileWriter = new PrintWriter(file, "UTF-8");

			for (int i = 0; i <= evolutionOperations.size(); i++) {
				StringBuilder lineBuilder = new StringBuilder();
				
				for(int j=0;j<=2;j++) {					
					if (j != 0) {
						lineBuilder.append(";");
					}
					
					lineBuilder.append(matrix[j][i]);
				}
				
				
				fileWriter.println(lineBuilder.toString());
			}
			
			fileWriter.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void writeShortGuidanceResultsToCSVOld(List<String> featureList, List<Configuration> configurations,
			List<EvolutionOperation> evolutionOperations, List<Guidance> guidanceList, File file) {

		long amountOfBehaviorPreservationOverall = 0;
		long amountOfBehaviorPreservationForMerge = 0;
		long amountOfBehaviorPreservationForDelete = 0;
		long amountOfBehaviorPreservationForExtract = 0;

		long amountOfD0 = 0;
		long amountOfD1 = 0;

		long amountOfM0 = 0;
		long amountOfM1 = 0;
		long amountOfM2 = 0;
		long amountOfM3 = 0;

		long amountOfE0 = 0;
		long amountOfE1 = 0;

		for (Guidance guidance : guidanceList) {
			if (guidance.isBehaviorPreserving()) {
				amountOfBehaviorPreservationOverall++;
			}

			EvolutionOperation evoOp = guidance.getEvolutionOperation();

			if (evoOp instanceof RemoveFeatureWithArtifacts) {
				if (guidance.isBehaviorPreserving()) {
					amountOfBehaviorPreservationForDelete++;
				}

				if (guidance.getGuidanceIdFromPaper().equals("D0")) {
					amountOfD0++;
				} else if (guidance.getGuidanceIdFromPaper().equals("D1")) {
					amountOfD1++;
				}
			} else if (evoOp instanceof MergeFeatures) {
				if (guidance.isBehaviorPreserving()) {
					amountOfBehaviorPreservationForMerge++;
				}

				if (guidance.getGuidanceIdFromPaper().equals("M0")) {
					amountOfM0++;
				} else if (guidance.getGuidanceIdFromPaper().equals("M1")) {
					amountOfM1++;
				} else if (guidance.getGuidanceIdFromPaper().equals("M2")) {
					amountOfM2++;
				} else if (guidance.getGuidanceIdFromPaper().equals("M3")) {
					amountOfM3++;
				}

			} else if (evoOp instanceof ExtractFeature) {
				if (guidance.isBehaviorPreserving()) {
					amountOfBehaviorPreservationForExtract++;
				}

				if (guidance.getGuidanceIdFromPaper().equals("E0")) {
					amountOfE0++;
				} else if (guidance.getGuidanceIdFromPaper().equals("E1")) {
					amountOfE1++;
				}
			}
		}

		try {

			PrintWriter writer = new PrintWriter(file, "UTF-8");

			StringBuilder lineBuilder = new StringBuilder();
			lineBuilder.append("AmountOfFeatures;");
			lineBuilder.append(featureList.size());
			writer.println(lineBuilder.toString());

			lineBuilder = new StringBuilder();
			lineBuilder.append("AmountOfConfigurations;");
			lineBuilder.append(configurations.size());
			writer.println(lineBuilder.toString());

			lineBuilder = new StringBuilder();
			lineBuilder.append("AmountOfEvolutionOperations;");
			lineBuilder.append(evolutionOperations.size());
			writer.println(lineBuilder.toString());

			lineBuilder = new StringBuilder();
			lineBuilder.append("AmountOfGuidances;");
			lineBuilder.append(guidanceList.size());
			writer.println(lineBuilder.toString());

			lineBuilder = new StringBuilder();
			lineBuilder.append("AmountOfBehaviorPreservationOverall;");
			lineBuilder.append(amountOfBehaviorPreservationOverall);
			writer.println(lineBuilder.toString());

			lineBuilder = new StringBuilder();
			lineBuilder.append("AmountOfBehaviorPreservationDelete;");
			lineBuilder.append(amountOfBehaviorPreservationForDelete);
			writer.println(lineBuilder.toString());

			lineBuilder = new StringBuilder();
			lineBuilder.append("AmountOfBehaviorPreservationMerge;");
			lineBuilder.append(amountOfBehaviorPreservationForMerge);
			writer.println(lineBuilder.toString());

			lineBuilder = new StringBuilder();
			lineBuilder.append("AmountOfBehaviorPreservationExtract;");
			lineBuilder.append(amountOfBehaviorPreservationForExtract);
			writer.println(lineBuilder.toString());

			lineBuilder = new StringBuilder();
			lineBuilder.append("AmountOfConfigurationsIn D0;");
			lineBuilder.append(amountOfD0);
			writer.println(lineBuilder.toString());

			lineBuilder = new StringBuilder();
			lineBuilder.append("AmountOfConfigurationsIn D1;");
			lineBuilder.append(amountOfD1);
			writer.println(lineBuilder.toString());

			lineBuilder = new StringBuilder();
			lineBuilder.append("AmountOfConfigurationsIn M0;");
			lineBuilder.append(amountOfM0);
			writer.println(lineBuilder.toString());

			lineBuilder = new StringBuilder();
			lineBuilder.append("AmountOfConfigurationsIn M1;");
			lineBuilder.append(amountOfM1);
			writer.println(lineBuilder.toString());

			lineBuilder = new StringBuilder();
			lineBuilder.append("AmountOfConfigurationsIn M2;");
			lineBuilder.append(amountOfM2);
			writer.println(lineBuilder.toString());

			lineBuilder = new StringBuilder();
			lineBuilder.append("AmountOfConfigurationsIn M3;");
			lineBuilder.append(amountOfM3);
			writer.println(lineBuilder.toString());

			lineBuilder = new StringBuilder();
			lineBuilder.append("AmountOfConfigurationsIn E0;");
			lineBuilder.append(amountOfE0);
			writer.println(lineBuilder.toString());

			lineBuilder = new StringBuilder();
			lineBuilder.append("AmountOfConfigurationsIn E1;");
			lineBuilder.append(amountOfE1);
			writer.println(lineBuilder.toString());

			writer.close();
		} catch (IOException e) {
			// do something
		}
	}

}
